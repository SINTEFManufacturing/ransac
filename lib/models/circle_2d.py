# coding=utf-8

"""
2D Circle model for RanSaC'ing.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2019-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import itertools
import dataclasses

import numpy as np
import math2d as m2d
import math2d.geometry as m2dg

from . import RanSaCModel


@dataclasses.dataclass
class Circle2DModel(RanSaCModel):
    """2D Circle model for RanSaC'ing."""
    centre: m2d.Vector
    radius: float
    
    cardinality: int = 3

    class Error(Exception):
        def __init__(self, msg):
            Exception.__init__(self, 'Circle2DModelError: ' + msg)
      
    def __post_init__(self):
        pass

    @classmethod
    def new_random_model(cls, ps,
                         res_tol: float,  # [m]
                         min_radius=1.0e-3, max_radius=10.0e-3, min_sep=0.5e-3,
                         max_attempts=1000):
        """Select the required number of points from 'ps' at random for
        fitting a model. 'min_sep' devices the minimal acceptable
        distance between the two points selected for representing the
        model. 'max_attempts' denotes the maximum number of attempts
        at finding an allowable model.
        """
        ps = np.array(ps)
        nps, dim,  = ps.shape
        if dim != 2:
            raise cls.Error(
                'new_random_model: '
                + 'Dimension must be 2 in row vectors. '
                + f'Got {dim}')
        m = None
        attempts = 0
        while m is None and attempts < max_attempts:
            attempts += 1
            # Sequentially select points within acceptable range
            # p0 = ps[np.random.randint(nps)]
            # p1ps = np.array([p for p in ps if np.linalg.norm(p-p0) < 2 * max_radius])
            # p12 = p1ps[np.random.choice(range(p1ps.shape[0]), 2, replace=False)]
            # mps = np.vstack((p0, p12))
            # Select model points independently
            mps = ps[np.random.choice(range(ps.shape[0]), 3, replace=False)]
            sep_ok = True
            for p1, p2 in itertools.combinations(mps, r=2):
                if np.linalg.norm(p1-p2) < min_sep:
                    sep_ok = False
                    break
            if sep_ok:
                try:
                    m = cls.new_fitted_points(mps)
                except cls.Error:
                    # print('Warning, errors in new_fitted_points')
                    continue
                if m.radius < min_radius or m.radius > max_radius:
                    m = None
        return m

    @classmethod
    def new_fitted_points(cls, ps: np.ndarray):
        """Select from ps, shape (3, dim), a set of points at random for
        forming a model.
        """
        ps = np.array(ps)
        nps, dim = ps.shape
        if dim != 2:
            raise Exception(
                'Circle.new_fitted_points: Must have points of '
                + f'dimension 2 as row vectors. Got {dim}!')
        if nps != 3:
            raise Exception(
                'Circle2D.new_fitted_points: Must have three points to fit.')
        # All point pairs form a chord of the model circle. Calculate
        # two chord centres and displacement vectors.
        cc1 = 0.5 * (ps[0] + ps[1])
        d1 = m2d.Vector(ps[1] - ps[0])
        cc2 = 0.5 * (ps[1] + ps[2])
        d2 = m2d.Vector(ps[2] - ps[1])
        # Form radial directions and lines
        r1 = d1.rotated(np.pi/2)
        r2 = d2.rotated(np.pi/2)
        l1 = m2dg.Line(cc1, cc1+r1)
        l2 = m2dg.Line(cc2, cc2+r2)
        # The circle centre is at the intersection of the radial lines
        isects = l1.intersection(l2)
        if len(isects) != 1:
            raise cls.Error('new_fitted_points: Dim=2, lines ' +
                            'along chord normals did not intersect! ' 
                            f'{ps}')
        c = isects[0]
        r = np.average(np.linalg.norm(ps - c, axis=1))
        return cls(c, r)

    def dists(self, ps):
        rel_ps = ps - self.centre
        return np.abs(np.linalg.norm(rel_ps, axis=1) - self.radius)
