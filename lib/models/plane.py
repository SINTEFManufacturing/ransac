# coding=utf-8

"""
Plane model for RanSaC'ing.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2019-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import numpy as np

from . import RanSaCModel


class PlaneModel(RanSaCModel):
    """Plane model for RanSaC'ing."""

    cardinality = 3

    def __init__(self, point, normal):
        self.p = point
        self.un = normal / np.linalg.norm(normal)
        self.dim = 3

    @classmethod
    def new_random_model(cls, ps, res_tol,
                         min_sep=1.0e-3, max_prod=0.1, max_attempts=1000):
        """Select the required three points from 'ps' at random for fitting a
        plane. 'min_sep' devises the minimal acceptable distance
        between pairs of the three points selected for representing
        the plane. 'max_prod' devises the maximum dot products between
        the two unit vectors for forming the plane normal. 'max_attempts'
        denotes the maximum number of attempts at finding an allowable
        model.
        """
        m = None
        nps, dim = ps.shape
        attempts = 0
        while m is None and attempts < max_attempts:
            attempts += 1
            points = ps[np.random.randint(0, nps, 3)]
            p0, p1, p2 = points
            v0 = p1 - p0
            uv0 = v0 / np.linalg.norm(v0)
            v1 = p2 - p0
            uv1 = v1 / np.linalg.norm(v1)
            if (min(np.linalg.norm(v0), np.linalg.norm(v1)) > min_sep and
                np.abs(np.dot(uv0, uv1)) < max_prod):
                return cls.new_fitted_model(points)
        return None

    @classmethod
    def new_fitted_model(cls, points):
        """Fit the model to the given 'points'. The number of points must be
        three.
        """
        p = points[0]
        n = np.cross(points[1] - p, points[2] - p)
        return cls(p, n)

    def dists(self, ps):
        """Calculate the model dist to all points in 'ps'."""
        rel_ps = ps - self.p
        return np.abs(rel_ps.dot(self.un))

    def projected_points(self, ps):
        """Calculate the projections on the plane of all points in 'ps'."""
        rel_ps = ps - self.p
        return self.p + (rel_ps - np.outer(self.un, self.un).dot(rel_ps.T).T)
