# coding=utf-8

"""
2D Circle model for RanSaC'ing.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2020"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import numpy as np
import math2d as m2d

from . import RanSaCModel


class Ellipse2DModel(RanSaCModel):
    """2D Ellipse model for RanSaC'ing. Important references:
    https://math.stackexchange.com/questions/632442/calculate-ellipse-from-points
    https://github.com/Yiphy/Ransac-2d-Shape-Detection
    http://www.ma.ic.ac.uk/~rn/distance2ellipse.pdf
    https://stackoverflow.com/questions/22959698/distance-from-given-point-to-given-ellipse
    """
