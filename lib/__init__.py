# coding=utf-8

"""RanSAC modlule. Reference:
https://en.wikipedia.org/wiki/Random_sample_consensus
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2019-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import logging
import typing

import numpy as np

from .models import RanSaCModel, LineModel

log = logging.getLogger('RanSaC')


# Structure for holding estimated models
# EModel = collections.namedtuple('EModel', ['m', 'rm', 'ins', 'cons', 'rem_ps'])
class EModel(typing.NamedTuple):
    m: RanSaCModel  # Winning model
    rm: RanSaCModel  # Regressed model, None if regression not specified
    ins: np.ndarray  # Inlier points [NxD]
    cons: np.ndarray  # Consumed points [NxD]
    # rem_ps: np.ndarray  # Remaining points

    def __repr__(self):
        return (f'<EstimatedModel: type={self.m.__class__.__name__} ' +
                f'#inliers={len(self.ins)} #consumed={len(self.cons)}>')

                
def ransac(ps: np.ndarray,
           model_type: RanSaCModel,
           model_kwargs: dict = {},
           n_iter: int = 10000,
           res_tol: float = 0.1,
           cons_tol: float = 1.0,
           regress: bool = False,
           max_unimp_count: int = 1000):
    """Find a single model by RanSaC'ing.

    ps: Array of either row or column vectors. Shape (n, dim)

    model_kwargs: Dict of arguments for creating random model.

    n_iter: The number of trials to perform before returning the
    best.

    res_tol: The residual tolerance for a point to be an inlier
    according to the trial model.

    cons_tol: The toleranse for final consuption of points in the
    model.

    regress: Flag for regression on final consumed set.

    max_unimp_count: Stopping criteria when a model has remained
    unimproved for this number of iterations.
    """
    # global best_cons_pts, m
    best_in_idxs = []
    best_m = None
    nps, dim = ps.shape
    unimp_count = 0
    # Add residual tolerance to model arguments
    for i in range(n_iter):
        log.debug(f'Iteration {i}')
        log.debug(f'{model_kwargs}')
        m = model_type.new_random_model(ps, res_tol, **model_kwargs)
        if m is None:
            break  # return None, ps.copy()
        # if m.latest_selection is None:
            # Model generation did not select mask and indices
        m_mask, m_idxs = m.select_points(ps, res_tol)
        # else:
        #     m_mask, m_idxs = m.latest_selection
        if len(m_idxs) > len(best_in_idxs):
            best_cons_mask, best_cons_idxs = m.select_points(ps, cons_tol)
            # best_cons_pts = ps[best_cons_idxs]
            best_in_idxs = m_idxs
            best_out_idxs = np.where(np.logical_not(best_cons_mask))[0]
            best_m = m
            unimp_count = 0
        else:
            unimp_count += 1
            if unimp_count > max_unimp_count:
                log.debug('Breaking after unimproved.')
                break
    if best_m is None:
        return None, ps
    else:
        log.debug(f'Finished after {i} iterations with {len(best_in_idxs)} inliers. Model: {m}')
        if regress and model_type == LineModel:
            if dim == 2:
                from math2d.geometry import Line
            elif dim == 3:
                from math3d.geometry import Line
            rl = Line.new_fitted_points(ps[best_cons_idxs])
            rm = LineModel.new_point_direction(rl.point.array,
                                               rl.unit_direction.array)
        else:
            rm = None
        return (EModel(best_m, rm,
                       ps[best_in_idxs],
                       ps[best_cons_idxs]),
                ps[best_out_idxs])


def multi_ransac(ps: np.ndarray,
                 n_models: int,
                 model_type: RanSaCModel,
                 model_kwargs: dict = {},
                 min_inliers: int = 10,
                 n_iter: int = 10000,
                 res_tol: float = 0.001,
                 cons_tol: float = 0.005,
                 regress: bool = False,
                 max_unimp_count: int = 1000):
    """Find a maximum of 'n_models' of the 'model_type' in the points in
    'ps'. Shape of 'ps': (n, dim).
    """
    emodels = []
    rem_ps = ps.copy()
    emodel = 'NotNone'
    while (emodel is not None
           and len(emodels) < n_models
           and rem_ps.shape[0] >= min_inliers):
        emodel, rem_ps = ransac(rem_ps, model_type, model_kwargs,
                                n_iter, res_tol, cons_tol,
                                regress, max_unimp_count)
        if emodel is not None:
            if emodel.ins.shape[0] >= min_inliers:
                emodels.append(emodel)
            else:
                emodel = None
    if emodel is None:
        log.debug('Breaking since a model could not be found.')
    elif emodel.ins.shape[0] < min_inliers:
        log.debug('Breaking due to inlier count (< {}).'.format(min_inliers))
    elif rem_ps.shape[0] <= min_inliers:
        log.debug(f'Breaking due to low count of remaining points (< {min_inliers}).')
    return emodels, rem_ps

"""



"""

#if __name__ == '__main__':
    # _test_line_model(dim=2, regress=True)
    # _test_circle2d_model()
    #_test_plane_model()
